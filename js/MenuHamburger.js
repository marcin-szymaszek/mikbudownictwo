$("nav div").click(function () {
   $("nav ul").slideToggle();
});

$(window).resize(function () {
    if ($(window).width() > 768) {
        $("nav ul").removeAttr('style');
    }
});