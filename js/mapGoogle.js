function initMap () {
    var myCenter = new google.maps.LatLng(50.047298, 19.992050);
    var mapCanvas = document.getElementById("map");
    var mapOptions = {
        center: myCenter,
        zoom: 15
    };
    var map = new google.maps.Map(mapCanvas, mapOptions);
    var marker = new google.maps.Marker({
        position: myCenter,
        animation: google.maps.Animation.DROP
    });
    marker.setMap(map);
}
